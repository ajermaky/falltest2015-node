
/*!
 * Module dependencies.
 */
var mongoose = require('mongoose');
 var Location = mongoose.model('Location');

exports.index = function (req, res) {
	console.log('heh');
  return Location.find().select('-__v').exec(function(err,locations){
  	if(err||!locations) return res.json({'status':'Cannot find objects'});

  	return res.json(locations);
  });
  
}

exports.create = function (req,res){
	var location = new Location(req.body);
	return location.save(function(err){
		if(err) return res.json({'status':'Creation unsuccessful'});

		return res.json({'status':'Creation successful'});
	});
}

exports.show = function(req,res){
	return Location.findById(req.params.id).select('-__v').exec(function(err,location){
		if(err||!location) return res.json({'status':'Cannot find object'});
		return res.json(location);
	});
}

exports.update= function (req,res){
	return Location.findById(req.params.id,function(err,location){
		if(err||!location) return res.json({'status':'Cannot find object'});

		location.person = req.body.person;
		location.victim = req.body.victim;
		location.alcohol = req.body.alcohol;
		location.weapon = req.body.weapon;
		location.longitude=req.body.longitude;
		location.latitude=req.body.latitude;
		location.date=req.body.date;

		return location.save(function (err) {
		if(err) return res.json({'status':'Update unsuccessful'});

		return res.json({'status':'Update successful'});

		})

	});
	
}

exports.destroy=function (req,res) {
	// body...
	return Location.findById(req.params.id,function(err,location){
		if(err||!location) return res.json({'status':'error: cannot find object'});
		
		return location.remove(function(err){
			if(err) return res.json({'status':'error: Destruction unsuccessful'});

			return res.json({'status':'Destruction successful'})
		});
		
	});


}



