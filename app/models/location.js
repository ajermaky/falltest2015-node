
/*!
 * Module dependencies
 */

var mongoose = require('mongoose');
var userPlugin = require('mongoose-user');
var Schema = mongoose.Schema;

/**
 * User schema
 */

var LocationSchema = new Schema({
  person: {type: String, default:'Joe'},
  weapon: {type: String, default:'Sniper'},
  alcohol: {type: Boolean, default:true},
  victim: {type: String, default:'Doe'}, 
  longitude: { type: Number, default:0},
  latitude: { type: Number, default:0},
  date: {type: Date, default: Date.now},
  explosives: {type:Boolean, default:true}

});

/**
 * User plugin
 */


/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

LocationSchema.method({
	

});

/**
 * Statics
 */

LocationSchema.static({

});

/**
 * Register
 */

mongoose.model('Location', LocationSchema);
